using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Input : MonoBehaviour
{
    public Slider Slider;
    public int Accuracy;
    public List<DifficultyController> _Controller = new List<DifficultyController>(0);
    public int AmountOfBranches = 13;
    public GameMaster master;
    public Basket Basket;
    private List<SpawnLocation> spawnloca = new List<SpawnLocation>(0);

    public void Answer()
    {
        AddToList();
        
        foreach (var script in spawnloca)
        {
            script.Spawn();
        }
        Basket. AddtoList();

        Accuracy  =  (int) (Slider.value - Slider.maxValue / 2);
        Debug.Log("My accuracy is: " + Accuracy);
       
        var level = GetLevel(Accuracy);
        
        foreach (var controller in _Controller)
        {
            
            if (Accuracy > 0)
            {
                controller.Setlevel(level);
              
                Debug.Log("links blind");
            }
            
            if(Accuracy < 0)
            {
                
                controller.SetlevelBad(level);
                
                Debug.Log("rechts blind");
            }
            
            Debug.Log("Current level: "+ level);
           
        }
        master.StartGame1();

     

    }

    public int GetLevel(int result)
    {
        //Accurcy vs distance Voor meer level maak gwn veel if of switches met result > set distance

        if (result > 29)
        {
            Debug.Log("A");
            return 1;
        }
           
        if (result > 19)
        {
            Debug.Log("B");
            return 2;
        }
        if (result >= 1)
        {
            Debug.Log("C");
            return 3;
        }

        if (result >= -1)
        {
            Debug.Log("AA");
            return 1;
            
        }

        if (result > -19)
        {
            Debug.Log("D");
            return 2;
        }
        if (result > -29)
        {
            Debug.Log("E");
            return 3;
        }
        
       
        
        Debug.Log("Doet");
        return 3;
        
    }

    public void AddToList()
    {
        _Controller.Clear();
        _Controller.Capacity = AmountOfBranches;
        for ( int i = 0; i < AmountOfBranches; i++)
        {
           
            _Controller.AddRange(gameObject.GetComponentsInChildren<DifficultyController>());
            _Controller = _Controller.Distinct().ToList();
            spawnloca.AddRange(gameObject.GetComponentsInChildren<SpawnLocation>());
            spawnloca = spawnloca.Distinct().ToList();

        }
    }
    
    
    
}
