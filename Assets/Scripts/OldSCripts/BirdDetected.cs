using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdDetected : MonoBehaviour
{ 
    
    
    
    public void birdIsDetected()
    {
        Debug.Log("Good");
        Destroy(gameObject);
    }

    public void badBirdIsDetected()
    {
        Debug.Log("BAD");
        Destroy(gameObject);
    }
}
