using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class BirdMovement : MonoBehaviour
{
    public float timeTakenDuringLerp = 8f;
    private bool _isLerping;
    private float _timeStartedLerping;
    
    
    private Vector3 _startPosition;
    private Vector3 _endPosition;
    
    private GameObject _startPoint;
    public List<GameObject> _endPoint = new List<GameObject>(0);

   
    public GameObject _thispoint;
    public GameObject Master;


    private void Start()
    {
        Master = GameObject.FindGameObjectWithTag("Master");
        _startPoint = GameObject.FindGameObjectWithTag("Basket");
        GiveRoute(Master);
        _startPosition = _startPoint.transform.position;
        GoMove();

    }


    public void GoMove()
    {
        _isLerping = true;
        _timeStartedLerping = Time.time;

        _startPosition = transform.position;
        int random = Random.Range(1, Master.transform.childCount);
        _endPosition = _endPoint[random].transform.position;
        _thispoint =_endPoint[random];
        
    }

    private void Update()
    {
        if (_endPosition == transform.position)
        {
            _thispoint.tag = "Bird";
            Debug.Log(_thispoint.tag + "I have become");
            Destroy((gameObject));
            
           

        }
    }

    public void GiveRoute(GameObject Master)
    { 
        
        //GameObject.FindWithTag("Checker")
        for (int i = 0; i < Master.transform.childCount; i++)
        {
          
            _endPoint.Add(Master.transform.GetChild(i).gameObject);
            
        }

    }


    private void FixedUpdate()
    {
        
        if (_isLerping)
        {
            var timeSinceStarted = Time.time - _timeStartedLerping;
            var percentageComplete = timeSinceStarted / timeTakenDuringLerp;

            transform.position = Vector3.Lerp(_startPosition, _endPosition, percentageComplete);

            if (percentageComplete >= 1.0f) _isLerping = false;
        }
    }
}