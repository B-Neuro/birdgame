using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationChecker : MonoBehaviour
{
    public Renderer _renderer;
    public Color _original;
    public Color _good;
    public Color _bad;

    public bool _goodHintStarted = false;
    public bool _badHintStarted = false;
   private void Awake()
    {
        _renderer = gameObject.GetComponent<Renderer>();
    }

    //can be switch case
    private void Update()
    {
        if (gameObject.tag == "Bird" && !_goodHintStarted)
        {
            StartCoroutine(GoodHint());
        }
        if (gameObject.tag == "EvilBird" &&  !_badHintStarted)
        {
            StartCoroutine(BadHint());
        }

        if (gameObject.tag == "Checker")
        {
            StopAllCoroutines();
            _renderer.material.color = _original;
            _goodHintStarted = false;
            _badHintStarted = false;
        }
      
      
    }

    public IEnumerator GoodHint()
    {
        _renderer.material.color = _original;
        _goodHintStarted = true;
        yield return new WaitForSeconds(5);
        _renderer.material.color = _good;
        yield return new WaitForSeconds(2);
        _renderer.material.color = _original;
        StartCoroutine(GoodHint());

    }
   
    public IEnumerator BadHint()
    {
        _renderer.material.color = _original;
        _badHintStarted = true;
        yield return new WaitForSeconds(5);
        _renderer.material.color = _bad;
        yield return new WaitForSeconds(2);
        _renderer.material.color = _original;
        StartCoroutine(BadHint());

    }
   
   
   
   
   
}