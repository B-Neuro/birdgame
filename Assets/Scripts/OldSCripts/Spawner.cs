using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public bool _birdSpawn;
    public bool _eviBirdSpawn;
    public GameObject Bird;
    public GameObject EvilBird;
    public bool level3done;
    public bool level4done = false;
    public int birdAmount;
     [Range(1, 3)]public int level;
    public int currentbird = 0;
    public GameMaster Master;
    private void Start()
    {
        switch (level)
        {
            case 1:
                birdAmount = 6;
                break;
            case 2:
                birdAmount = 8;
                break;
            case 3:
                birdAmount = 10;
                break;
            
        }
    }


    private void Update()
    {
        if (currentbird == birdAmount)
        {


            _eviBirdSpawn = true;
            
            if (level3done)
            {
                Debug.Log("START GAME 3");
                Master.StartGame3();
                
                if (level4done)
                {
                    Debug.Log("sTART GAME 4");
                    BirdGameDone();
                }
               level4done = true;

            }
            level3done = true;
           
            
        }
    }

    public void resetBird()
    {
        currentbird = 0;
    }

    public void BirdSpawn()
    {
        Debug.Log("Currently at: " + currentbird);
        if (_birdSpawn)
        {
            if (currentbird >= birdAmount) return;
            currentbird += 1;
            Instantiate(Bird, gameObject.transform.position, Quaternion.identity);
            Debug.Log("I should have spawned?");   
        }

        
    }

    public void BirdGameDone()
    {
        _birdSpawn = false;
        _eviBirdSpawn = false;
        Master.StartGame5();
    }

    public void EvilBirdSpawn()
    {
        Debug.Log("Currently at: " + currentbird);
        if (_eviBirdSpawn)
        {
            if (currentbird < birdAmount)
            {
                Instantiate(EvilBird, gameObject.transform.position, Quaternion.identity);
            }
        }
    }
}