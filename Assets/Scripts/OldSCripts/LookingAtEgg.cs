using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class LookingAtEgg : MonoBehaviour
{

    public Spawner _spawner;
    private RaycastHit _look;

    private Detected _detected;

    private BirdDetected _birdDetected;

    //public scoreboard scoreboard


    void Update()
    {

        if (Physics.Raycast(transform.position, transform.forward, out _look))
        {

            if (_look.transform.tag == "Egg")
            {
                //Detected.isDetected();
                _detected = _look.collider.GetComponent<Detected>();
                _detected.isDetected();

            }

           
            if (Physics.Raycast(transform.position, transform.forward, out _look) == false)
            {
                Debug.Log("kijkt naar niks");

            }

        }
    }
}