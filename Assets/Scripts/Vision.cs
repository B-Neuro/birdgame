using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour
{
    private RaycastHit _look;
    private Detected _detected;
    public Spawner _spawner;
    private BirdDetected _birdDetected;

    void Update()
    {
        if (Physics.Raycast(transform.position, transform.forward, out _look))
        {
            if (_look.transform.tag == "Egg")
            {
                _detected = _look.collider.GetComponent<Detected>();
                _detected.isDetected();
                
            }
            
            if (_look.transform.tag == "Ball")
            {
                _detected = _look.collider.GetComponent<Detected>();
                _detected.isDetected();
                
            }
            if (_look.transform.tag == "Bird")
            {

                _birdDetected = _look.collider.GetComponent<BirdDetected>();
                if (_birdDetected != null)
                {
                    Debug.Log("Kijkt Naar vogel");
                    _birdDetected.birdIsDetected();
                    _spawner.BirdSpawn();
                }

                if (_birdDetected == null)
                {
                    
                    _look.transform.tag = "Checker";
                    _spawner.BirdSpawn();

                }
                
            }
            if (_look.transform.tag == "EvilBird")
            {
                Debug.Log("Kijkt Naar slechte vogel vogel");

                _birdDetected = _look.collider.GetComponent<BirdDetected>();
                if (_birdDetected != null)
                {
                    _birdDetected.badBirdIsDetected();
                    _spawner.EvilBirdSpawn();
                }

                if (_birdDetected == null)
                {

                    _look.transform.tag = "Checker";
                    _spawner.EvilBirdSpawn();

                }
            }
            else
            {
                Debug.Log("Kijkt naar andere niks");
            }

            
            

        }
    }
}
