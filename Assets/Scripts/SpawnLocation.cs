using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnLocation : MonoBehaviour
{
    public GameObject egg;
    private GameObject Collector;
    public GameObject myeggpub;
    private Input _input;
    private Basket _basket;
    
    
    


    private void Awake()
    {
        Collector = GameObject.FindWithTag("Basket");
        _basket = Collector.GetComponent<Basket>();
    }

    public void Spawn()
    {
        var pos = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
        GameObject myegg = Instantiate(egg, pos, Quaternion.Euler(-90f, 0, 0),Collector.transform);
        myeggpub = myegg;
        _basket.currenteggs++;
    }

   
}
