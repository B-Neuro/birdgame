using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TextCore;

public class DifficultyController : MonoBehaviour
{

    public int ActiveForm;
    private SpawnLocation _spawnLocation;
    private GameObject basket;
    private Basket basketscript;
    

    private void Awake()
    {
        _spawnLocation = gameObject.GetComponent<SpawnLocation>();
        basket = GameObject.FindWithTag("Basket");
        
        basketscript = basket.GetComponent<Basket>();
    }

    public void SetActivefromLevel(int startLevel)
    {
        ActiveForm = startLevel;
    }
    //this is for left side
    public void Setlevel(int level)
    {
        if(level < ActiveForm)
            TurnMeOff();
        
    }
    //this is for right side
    public void SetlevelBad(int level)
    {
        if(level > ActiveForm)
            TurnMeOff();

    }
    
    
    public void TurnMeOff()
    {
        basketscript.currenteggs--;
        basketscript.Notifications.Remove(_spawnLocation.myeggpub.GetComponent<EggNotification>());
        Destroy(_spawnLocation.myeggpub);
      
        Debug.Log("Go through?");
    }
    
    
}
