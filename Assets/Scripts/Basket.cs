using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
public class Basket : MonoBehaviour
{
    public int TotalEggs = 12;
    public int currenteggs;
    public Input Input;
    public int lives;
    public GameMaster master;
    public List<EggNotification> Notifications = new List<EggNotification>(0);
    
    

    public void AddtoList()
    {
        Debug.Log("GO");
        Input.AmountOfBranches = currenteggs;
        Notifications.Capacity = currenteggs;

        Notifications.Capacity = currenteggs;
        for (int i = 0; i < currenteggs; i++) //totaleggs for older version
        {
            Notifications.AddRange(gameObject.GetComponentsInChildren<EggNotification>());
            Notifications = Notifications.Distinct().ToList();
        }
    }

    public void popEgg()
    { 
        currenteggs -= 1;
        Input.AmountOfBranches = currenteggs;

    }

    public void Hint()
    {
        Debug.Log($"I Have this many eggs: {currenteggs}");
        foreach (var eggnoty in Notifications)
        {
            eggnoty.Notify();
                    
        }
        
    }

    public void Checkammount()
    {
        if (lives > 0)
        {
            if (currenteggs == 0)
            {
                Debug.Log("Should be good");
                master.StartGame2();
            }
            else
            {
                Debug.Log("WRONG");
                lives -= 1;
                
                Debug.Log("Current amount of eggs: " + currenteggs);
                foreach (var eggnoty in Notifications)
                {
                    eggnoty.Notify();
                    
                }
            }
        }

    }
}

        
