using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{

    public GameObject part1;
    public GameObject part2;
    public Spawner _spawned;

    private void Start()
    {  
        part1.SetActive(true);
        part2.SetActive(false);
        
    }
    public void StartGame1()
    {
        part2.SetActive(true);
        part1.SetActive(false);

    }

    public void StartGame2()
    {
      
        part2.SetActive(false);
        part1.SetActive(false);
        _spawned._birdSpawn = true;
        _spawned.BirdSpawn();

    }
    public void StartGame3()
    {
        _spawned.resetBird();
        _spawned._eviBirdSpawn = true;
        _spawned.EvilBirdSpawn();
        
    }

    public void StartGame5()
    {
        Debug.Log("FINISHED");
    }




}