using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EggNotification : MonoBehaviour
{
    //public AudioSource MySound;

    public void Notify()
    {
        if (gameObject == true)
        {
            StartCoroutine(Notification());
        }
    }

    public IEnumerator Notification()
    {
        yield return new WaitForSeconds(2);
        transform.Translate(Vector3.forward);
        //do stuff
        //MySound.Play();
        yield return new WaitForSeconds(2);
        transform.Translate(Vector3.back);
    }
}