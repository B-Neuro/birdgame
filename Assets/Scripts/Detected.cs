using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Detected : MonoBehaviour
{
    private Basket Basket;
    private GameObject container;
    private EggNotification EggNoti;
    private void Start()
    {
        container = GameObject.FindWithTag("Basket");
        Basket = container.GetComponent<Basket>();
        EggNoti = gameObject.GetComponent<EggNotification>();
    }

    public void isDetected()
    { 
        Basket.popEgg();
        Basket.Notifications.Remove(EggNoti);        
        Debug.Log("Ive been seen");
        Destroy(gameObject);
        
    }
}