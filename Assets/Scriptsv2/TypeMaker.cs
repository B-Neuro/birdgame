using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TypeMaker : MonoBehaviour
{
    public GameObject Selected;
    public GameObject correct;

    private SelectMode _selectMode;
    private MeshRenderer _renderer;
    public Material[] _material;
    private int rendercolor;
    public bool wrong = false;
    public bool selected = false;
    
    private void Awake()
    {
         _selectMode = GetComponentInParent<SelectMode>();
        _renderer = GetComponent<MeshRenderer>();
        rendercolor = Random.Range(0, 4);
        _selectMode.Submitanswer += Answerd;

        
        switch (rendercolor)               
        {
            case 0:
                tag = "Charmender";
                _selectMode.Char.Add(gameObject);
                break;
            case 1:
                tag = "Patrick";
                _selectMode.Pat.Add(gameObject);
                break;
            case 2:
                //fout gespeld ook in tags btw
                tag = "Pingiun";
                _selectMode.Pin.Add(gameObject);
                break;
            case 3:
                tag = "Amogus";
                _selectMode.Amogus.Add(gameObject);
                break;
            
        }
        _renderer.material = _material[rendercolor];
    }

    private void OnMouseDown()
    {
        Selected.SetActive(true);
        
        if (_selectMode.Mamogus && gameObject.tag == "Amogus")
        {
            _selectMode.intAmogus++;
            selected = true;
        }
        if (!_selectMode.Mamogus && gameObject.tag == "Amogus")
        {
            wrong = true;
        }
        if (_selectMode.Mchar && tag == "Charmender")
        {
            _selectMode.intChar++;
            selected = true;

        }
        if (!_selectMode.Mchar && tag == "Charmender")
        {
          
            wrong = true;
        }
        if (_selectMode.Mpat&& tag == "Patrick")
        {
            _selectMode.intPat++;
            selected = true;
        }
        if (!_selectMode.Mpat&& tag == "Patrick")
        {
            wrong = true;
        }
        
        if (_selectMode.Mpin&& tag == "Pingiun")
        {
            _selectMode.intPin++;
            selected = true;
        }
        if (!_selectMode.Mpin&& tag == "Pingiun")
        {
            wrong = true;
        }
    }

    public void Answerd()
    {
        
        

        if (!wrong && selected)
        {
            correct.SetActive(true);
        }
        if (!wrong && !selected)
        {
            if (_selectMode.Mamogus && tag == "Amogus")
            {
                _selectMode.missed.Add(gameObject);
            }
            if (_selectMode.Mchar && tag == "Charmender")
            {
                _selectMode.missed.Add(gameObject);
            }
            if (_selectMode.Mpat && tag == "Patrick")
            {
                _selectMode.missed.Add(gameObject);
            }
            if (_selectMode.Mpin && tag == "Pingiun")
            {
                _selectMode.missed.Add(gameObject);
            }
            
            
           
        }
        
        
        
        
        Selected.SetActive(false);

        wrong = false;

    }
    
    
}
