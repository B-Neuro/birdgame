using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class gridspawner : MonoBehaviour
{

    public int Xval;
    public int Yval;
    public float distancebetweenobj;
    private int spawnedtimes;
    
    public GameObject obj;
    // Start is called before the first frame update
    void Start()
    {
        spawngrid(Xval, Yval);



    }

    public void spawngrid(int x, int y)
    {
        for (float i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                spawnedtimes++;

                GameObject DiagnoseOBJ = Instantiate(obj, Vector3.zero, obj.transform.rotation,transform);
                DiagnoseOBJ.transform.localPosition = new Vector3(i * distancebetweenobj, j * distancebetweenobj, 0);
            }
        }
       // Debug.Log($"I spawned this many times: {spawnedtimes}");
    }
}
