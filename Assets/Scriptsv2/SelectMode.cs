using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class SelectMode : MonoBehaviour
{
    public GameObject Screen;
    public gridspawner _Gridspawner;
    public GameObject border;
    
    public bool Mamogus, Mchar, Mpin, Mpat = false;
   //^Morgen naar Switch 

   public int intAmogus;
   public int intChar;
   public int intPin;
   public int intPat;
   public Action Submitanswer;
   public int gamemodeid = 0;

   public Text Screentext;

   public List<GameObject> Amogus = new List<GameObject>();
   public List<GameObject> Char = new List<GameObject>();
   public List<GameObject> Pin = new List<GameObject>();
   public List<GameObject> Pat = new List<GameObject>();

   public GameObject HighXLowY;
   public GameObject LowXLHighY;
   
   private float highestX = float.MinValue;
   private float lowestY = float.MaxValue;
   
   private float lowestX = float.MaxValue;
   private float highestY = float.MinValue;

   public List<GameObject> missed = new List<GameObject>();
   
   

   private void Start()
   {
       
       Screentext.text = "Select: Amongus";
       Mamogus = true;
       Submitanswer += SubmitAnswerFunc;
   }

   public void Answer()
   {
       Submitanswer();
   }
   

   private void SubmitAnswerFunc()
   {
      
      
      // Debug.Log(gamemodeid);
       if (Mamogus && gamemodeid == 0)
       {
           Screentext.text = "Select: Charmender";
           if (intAmogus == Amogus.Count)
           {
              // Debug.Log("Perfect Amogus ");
           }

           Mchar = true;
           Mamogus = false;
       }
       if (Mchar&& gamemodeid == 1)
       {
           Screentext.text = "Select: Pinguin";
           if (intChar == Char.Count)
           {
               //Debug.Log("Perfect Charmender ");
           }

           Mpin = true;
           Mchar = false;
       }
       if (Mpin && gamemodeid == 2)
       {
           Screentext.text = "Select: Patrick";
           if (intPin == Pin.Count)
           {
               //Debug.Log("Perfect Pinguin");
           }

           Mpat = true;
           Mpin = false;

       }
       if (Mpat && gamemodeid == 3)
       {
           Screentext.text = "Select: Ended";
           if (intPat == Pat.Count)
           {
               //Debug.Log("Perfect Patrick ");
           }
           Debug.Log("END GAME");
           
           foreach (var obj in missed)
           {
               float x = obj.transform.position.x;
               float y = obj.transform.position.y;

               if (x > highestX || (x == highestX && y < lowestY))
               {
                   highestX = x;
                   lowestY = y;
                   HighXLowY = obj;
               }
               if (x < lowestX || (x == lowestX && y > highestY))
               {
                   lowestX = x;
                   highestY = y;
                   LowXLHighY = obj;
               }
              
           }
           SummonField();
           
       }
       gamemodeid++;
    
   }

   public void SummonField()
   {
       var xloc = highestX + lowestX;
       var yloc = highestY + lowestY;
       var xscal = (HighXLowY.transform.localPosition.x  - LowXLHighY.transform.localPosition.x) /_Gridspawner.distancebetweenobj + 1f;
       var yscal =  (LowXLHighY.transform.localPosition.y  - HighXLowY.transform.localPosition.y) /_Gridspawner.distancebetweenobj + 1f;
       
       Debug.Log(HighXLowY.transform.localPosition.y);
       Debug.Log(LowXLHighY.transform.localPosition.y);

       Debug.Log($"x = {xscal}");
       Debug.Log($"Y = {yscal}");
     
      GameObject newscreen = Instantiate(Screen, new Vector3(xloc/ 2, yloc/ 2, -2.3f), Screen.transform.rotation);
      newscreen.transform.localScale = new Vector3(xscal * _Gridspawner.distancebetweenobj , 0, yscal * _Gridspawner.distancebetweenobj);
      //newscreen.transform.localScale = new

   }
   


  

    
}
