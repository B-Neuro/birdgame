using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Onclickscript : MonoBehaviour
{
    public SelectMode SelectMode;
    
    private void Update()
    {
        if (UnityEngine.Input.GetMouseButtonDown(0))
        {
            RaycastHit raycastHit;
            Ray ray = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
            if (Physics.Raycast(ray, out raycastHit, 100f))
            {
                if (raycastHit.transform != null)
                {
                    ClickedOBJ(raycastHit.transform.gameObject);
                }
            }

        }
    }


    public void ClickedOBJ(GameObject OBJ)
    {
        if (gameObject.tag == "Amogus")
        {
            SelectMode.Amogus.Remove(OBJ);
            
        }
        if (gameObject.tag == "Patrick")
        {
            SelectMode.Amogus.Remove(OBJ);
        }
        if (gameObject.tag == "Pingiun")
        {
            SelectMode.Amogus.Remove(OBJ);
        }
        if (gameObject.tag == "Charmender")
        {
            SelectMode.Amogus.Remove(OBJ);
        }
    }
}